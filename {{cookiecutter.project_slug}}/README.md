# {{cookiecutter.project_name}}

{{cookiecutter.project_short_description}}

## Usage

## Installation

    python3 -m pip install git+{{cookiecutter.git_url}}.git

## Credits

This package was created with
[Cookiecutter](https://github.com/audreyr/cookiecutter) and the
[zanfar/cookiecutter-pypackage](https://gitlab.com/zanfar/cookiecutter-pypackage)
project template.
