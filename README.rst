====================================
Python Package Cookiecutter Template
====================================

This repository has been moved to GitHub and is now DEPRECIATED.

Please use https://github.com/therealzanfar/cookiecutter-pypackage instead.

This repository will be removed at a future date.

Switching
---------

Simply re-downloading the template from GitHub will replace any existing
`cookiecutter` references:

    cookiecutter gh:therealzanfar/cookiecutter-pypackage

To change a repo checkout:

    git remote rm origin
    git remote add origin git@github.com:therealzanfar/cookiecutter-pypackage.git

or

    git remote rm origin
    git remote add origin https://github.com/therealzanfar/cookiecutter-pypackage.git

